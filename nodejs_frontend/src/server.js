function route(app, regex, prefix) {
 app.get(regex, function (req, res) {
 var type = req.params[0];
 var path = req.params[1];
 var file = prefix + type + '/' + path
 res.sendfile(file);
 });
}

var express = require('express');


var PORT = 8083;

var app = express();

app.get('/', function(req, res) {
 res.sendfile('index.html');
});

app.get('/login', function(req, res) {
 res.sendfile('login.html');
});
app.get('/menu', function(req, res) {
 res.sendfile('menu.html');
 });

app.get('/admin', function(req, res) {
 res.sendfile('admin.html');
});

app.get('/producto', function(req, res) {
 res.sendfile('producto.html');
});

app.get('/compra', function(req, res) {
 res.sendfile('compra.html');
});
app.get('/pedido', function(req, res) {
 res.sendfile('pedido.html');
});

route(app, /^\/(css|js|images|fonts)\/(.*)/, './');

app.listen(PORT);
console.log('Running on port ' + PORT);

