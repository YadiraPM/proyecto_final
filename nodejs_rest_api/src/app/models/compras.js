//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PedidohSchema = new Schema({
	ped : String,
	fech : String,
	clien : String,
	tel : String
});

module.exports = mongoose.model('Pedidoh', PedidohSchema);