//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductosSchema = new Schema({
	cod : String,
	nom : String,
	prec : String,
	exis : String
});

module.exports = mongoose.model('Productos', ProductosSchema);