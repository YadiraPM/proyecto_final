// server.js

// base setup

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/MARYKAY')
var Clientes = require('./app/models/clientes');
// call the packages


var express = require('express')
var app = express();
var bodyParser = require('body-parser');

// config app to use bodyParser()

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8081; 

// routes for our api

var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('something is happen..');
  

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
  
  res.header('Access-Control-Allow-Headers', 'Content-Type');


	next();  // make sure we go to the next routes
});


// test route

router.get('/', function(req, res) {
 res.json({ message: 'yahoo!! welcome to our api !' });
});

// more routes for our API will happen here
router.route('/clientes')
 
  // create a bear accessed at POST
  // http://localhost:8081/api/bears
 .post(function (req, res) {
 	var clientes = new Clientes();
	clientes.sex = req.body.sex;
	clientes.nom = req.body.nom;
    clientes.dire = req.body.dire;
	clientes.tel = req.body.tel


 	// save the bear and check for errors
  clientes.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Clientes created !'});
 	});

 	// body...
 })
  
 // get all the bears (accessed at 
 // http://localhost:8081/api/bears)
 .get(function (req, res) {

 	// body...
 	Clientes.find(function(err, clientes) 
 	{
 		if (err)

 			res.send(err);
 		res.json(clientes);
 	});
 });

// on routes that end in /bears/:bear_id
router.route('/clientes/:clientes_id')
 // get the bear with that id 
 // accessed at GET 
 // http://localhost:8081/api/bears/:bear_id

 .get(function(req, res) {
 	// body...
 	Clientes.findById(req.params.clientes_id, function(err, clientes)
 	{
 		if (err)
 			res.send(err);
 		res.json(clientes);

 	});
  })

 // update the bear with this id
 // accessed at PUT
 // http://localhost:8081/api/bears/:bear_id

 .put(function (req, res) {
 	// use our bear model to find the bear we want
 	Clientes.findById(req.params.clientes_id, function(err, clientes) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		clientes.sex = req.body.sex;
        clientes.nom = req.body.nom;
 		clientes.dire = req.body.dire;
        clientes.tel = req.body.tel;

 		// save the bear

 		clientes.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Clientes updated !'});
 		});
 	});
  })

 // delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8081/api/bears/:bear_id
 .delete(function (req, res) {
   Clientes.remove({
   	_id : req.params.clientes_id
   }, function (err, clientes) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Clientes deleted !'});
   });

 });

 

// register our routes
app.use('/api', router);
//app.use(require('cors')());

// start the server

app.listen(port);
console.log('Magic happens on port: ' + port);
