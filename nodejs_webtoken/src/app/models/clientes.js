//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientesSchema = new Schema({
	sex : String,
	nom : String,
	dire : String,
	tel : String
});

module.exports = mongoose.model('Clientes', ClientesSchema);