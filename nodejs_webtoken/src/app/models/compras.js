//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ComprasSchema = new Schema({
	ped : String,
	fech : String,
	clien : String,
	tel : String
});

module.exports = mongoose.model('Compras', ComprasSchema);