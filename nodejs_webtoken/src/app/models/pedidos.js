//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PedidosSchema = new Schema({
	idped : String,
	codp : String,
	cant : String,
	prec : String
});

module.exports = mongoose.model('Pedidos', PedidosSchema);